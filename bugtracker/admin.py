from django.contrib import admin
from bugtracker.models import Ticket

admin.site.register(Ticket)